module "gke" {
  source  = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  version = "~> 30.0"

  project_id = var.project_id
  name       = var.cluster_name
  regional   = false
  region     = var.region
  zones      = slice(var.zones, 0, 1)

  network                   = module.gcp-network.network_name
  subnetwork                = module.gcp-network.subnets_names[0]
  ip_range_pods             = var.ip_range_pods_name
  ip_range_services         = var.ip_range_services_name
  create_service_account    = true
  enable_private_endpoint   = true
  enable_private_nodes      = true
  master_ipv4_cidr_block    = "172.16.0.0/28"
  default_max_pods_per_node = 20
  remove_default_node_pool  = true
  deletion_protection       = false

  node_pools = [
    {
      name            = "np-1"
      min_count       = 1
      max_count       = 8
      local_ssd_count = 0
      disk_size_gb    = 10
      machine_type    = "e2-small"
      spot            = true

    }
  ]

  master_authorized_networks = [
    {
      cidr_block   = var.vpc_cidr
      display_name = "VPC"
    },
  ]
}
