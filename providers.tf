provider "google" {
  project = "keiko23"
  region  = "australia-southeast1"

  default_labels = {
    Terraform = "true"
  }
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}
