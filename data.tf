data "google_client_config" "default" {}

data "google_compute_network" "subnetwork" {
  name    = var.subnetwork
  project = var.project_id

  depends_on = [module.gcp-network]
}
